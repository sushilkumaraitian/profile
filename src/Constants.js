export const BaseUrl = 'https://playerprofile-poc-api.azurewebsites.net/';
export const ListingEndpoint = 'Player/api/v1/list';
export const ChampionshipEndpoint = 'Player/api/v1/championship';
export const PlayerProfileEndpoint = '/Player/api/v1/details?id='