import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Search from "../views/Search/Search";
import Merge from "../views/Merge/Merge";
import ResetPassword from "../views/ResetPassword/ResetPassword";
import PlayerProfile from "../views/PlayerProfile/PlayerProfile";

export const NavRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" name="Search" element={<Search />} />
        <Route exact name="Merge" path="/merge" element={<Merge />} />
        <Route
          exact
          name="Reset Password"
          path="/reset"
          element={<ResetPassword />}
        />
        <Route
          exact
          name="Profile"
          path="/profile/:profileId"
          element={<PlayerProfile />}
        />
      </Routes>
    </BrowserRouter>
  );
};
