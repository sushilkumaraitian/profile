import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  profiles: [],
  selectedProfiles: {},
  championships: [],
  selectedChampionship: {},
  query: {
		"PageNumber": 1,
		"PageSize": '',
		"OrderByColumn": "",
		"OrderByDir": "",
		"SearchFilters": []
	}
}

export const profileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    setProfiles: (state, action) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.profiles = action.payload;
    },
    selectProfiles: (state, action) => {
        state.selectedProfiles = action.payload;
    },
    setChampionships: (state, action) => {
      state.championships = action.payload
    },
    setSelectedChampionship: (state, action) => {
      state.selectedChampionship = action.payload
    },
    setQuery: (state, action) => {
      state.query = action.payload;
    }
  },
})

// Action creators are generated for each case reducer function
export const { setProfiles, selectProfiles, setChampionships, setSelectedChampionship, setQuery } = profileSlice.actions;

export default profileSlice.reducer;