import React from "react";
import Box from "@mui/material/Box";
import { DashboardLayout } from "../../containers/Layout/Layout";

const ResetPassword = () => {
  return (
    <DashboardLayout>
      <Box
        sx={{ m: 1, mb: 0 }}
        className="action-container margin-auto"
        style={{ border: "none" }}
      >
        <h2 className="mb-4">No content to display.</h2>
      </Box>
    </DashboardLayout>
  );
};

export default ResetPassword;
