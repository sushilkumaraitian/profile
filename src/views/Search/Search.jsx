import React, { useState, useEffect } from "react";
import { DashboardLayout } from "../../containers/Layout/Layout";
import { PlayerTable } from "../../components/PlayerTable/PlayerTable";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import CallMergeOutlinedIcon from '@mui/icons-material/CallMergeOutlined';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';
import IconButton from "@mui/material/IconButton";
import Tooltip from '@mui/material/Tooltip';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import { useSelector, useDispatch } from 'react-redux';
import { setProfiles, setSelectedChampionship, setQuery } from '../../state/profileSlice';
import { useNavigate } from "react-router-dom";


const Search = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { championships, selectedChampionship, query, profiles } = useSelector((state) => state.profile);
  const [selectedProfiles, setSelectedProfiles] = useState([]);
  const years = selectedChampionship.Years || [];
  const [selectedYear, setYear] = useState('');
  const [selectedChamp, setChamp] = useState('');
  const handleChampionshipChange = (event) => {
    setChamp(event.target.value);
    dispatch(setSelectedChampionship(event.target.value));
  };
  
  const handleYearChange = (event) => {
    setYear(event.target.value);
  };

  const searchData = (event) => {
    dispatch(setQuery({...query, champCode: selectedChampionship.ChampionShipName.Key, champYear: selectedYear}));
  };

  const clearData = (event) => {
    dispatch(setQuery({...query, champCode: '', champYear: ''}));
    dispatch(setSelectedChampionship({}));
    setYear('');
    setChamp('');
  };

  const gotoProfile = () => {
    if(selectedProfiles.length === 1){
      navigate("/profile/" + selectedProfiles[0].Id, { replace: true });
    }
  };

  const editProfile = () => {
    if(selectedProfiles.length === 1){
      navigate("/profile/" + selectedProfiles[0].Id + "?isEdit=true", { replace: true });
    }
  };

  useEffect(() => {
		setSelectedProfiles(profiles.filter(p => p.isSelected));
	}, [profiles]);

  return (
    <DashboardLayout>
      <div className="search-area margin-auto">
      <h3 className="search-heading margin-auto">Search</h3>
        <FormControl sx={{ width: '40%', marginTop: 3, marginLeft: 4, background: 'white' }}>
          <InputLabel id="demo-simple-select-label">Select a Championship</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Select a Championship"
            value={selectedChamp}
            onChange={handleChampionshipChange}
          >
            {championships.map((championship) => (
              <MenuItem
                key={championship.ChampionShipName.Key}
                value={championship}
              >
                {championship.ChampionShipName.Value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl sx={{ width: '20%', marginTop: 3, marginLeft: 4, background: 'white' }}>
          <InputLabel id="demo-simple-select-label">Year</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Year"
            value={selectedYear}
            onChange={handleYearChange}
          >
            {years.map((year) => (
              <MenuItem
                key={year}
                value={year}
              >
                {year}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button onClick={searchData} disabled={!selectedChampionship.ChampionShipName} variant="contained" size="medium" sx={{ width: '15%', marginLeft: 'calc(10% - 60px)',  marginTop: '5px', height: 56, background:"#005288" }}>
          Search
        </Button>
        <Button onClick={clearData} disabled={!query.champCode} variant="outlined" size="medium" sx={{ width: '15%', marginLeft: 2, marginTop: '5px', background: 'white', height: 56 }}>
          Clear
        </Button>
      </div>
      <div className="action-container margin-auto">
        <h3>Players</h3>
        <div className="action-container-icons">
          <span>
            <Tooltip title="Add" placement="top">
              <span>
                <IconButton disabled={true}>
                  <AddOutlinedIcon />
                </IconButton>
              </span>
            </Tooltip>
          </span>
          <span>
            <Tooltip title="View" placement="top">
              <span onClick={gotoProfile}>
                <IconButton disabled={selectedProfiles.length !== 1}>
                  <RemoveRedEyeOutlinedIcon />
                </IconButton>
              </span>
            </Tooltip>
          </span>          
          <span>
            <Tooltip title="Edit" placement="top">
              <span onClick={editProfile}>
                <IconButton disabled={selectedProfiles.length !== 1}>
                  <EditOutlinedIcon />
                </IconButton>
              </span>
            </Tooltip>
          </span>
          <span>
            <Tooltip title="Delete" placement="top">
              <span>
                <IconButton disabled={selectedProfiles.length === 0}>
                  <DeleteOutlineIcon />
                </IconButton>
              </span>
            </Tooltip>
          </span>
          <span>
            <Tooltip title="Merge" placement="top">
              <span>
                <IconButton disabled={selectedProfiles.length < 2}>
                  <CallMergeOutlinedIcon />
                </IconButton>
              </span>
            </Tooltip>
          </span>
        </div>
      </div>
      <div className="margin-auto">
        <PlayerTable />
      </div>
    </DashboardLayout>
  )
}

export default Search;
