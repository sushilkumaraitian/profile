import React, { useState, useEffect } from "react";
import {
  styled,
  useTheme,
  createTheme,
  ThemeProvider,
} from "@mui/material/styles";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Tooltip from "@mui/material/Tooltip";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import TextField from '@mui/material/TextField';
import OutlinedInput from "@mui/material/OutlinedInput";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import IconButton from "@mui/material/IconButton";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import Stack from "@mui/material/Stack";
import { DashboardLayout } from "../../containers/Layout/Layout";
import { useParams, useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";

import player from "../../assets/img/player.jpg";
import "./PlayerProfile.css";
import axios from "axios";
import { BaseUrl, PlayerProfileEndpoint } from "../../Constants";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 2 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

// const useStyles = makeStyles((theme) => ({
//   tabs: {
//     "& .MuiButtonBase-root.MuiTab-root:hover": {
//       backgroundColor: "red",
//     },
//   },
// }));

const CustomTabs = styled(Tabs)(({ theme }) => ({
  "& .MuiButtonBase-root.MuiTab-root:hover": {
    backgroundColor: "#F3F9FF",
  },
}));

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  minHeight: "36px",
  backgroundColor:
    theme.palette.mode === "dark" ? "rgba(255, 255, 255, .05)" : "#f7f7f9",
  marginTop: theme.spacing(5),
  flexDirection: "row-reverse",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)",
  },
  "& .MuiAccordionSummary-content": {
    margin: theme.spacing(0),
    marginLeft: theme.spacing(1),
    "& .MuiTypography-root": {
      fontSize: "13px",
      fontWeight: "bold",
    },
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
}));

const PlayerProfile = (props) => {
  let { profileId } = useParams();
  const search = useLocation().search;
  const theme = useTheme();
  let isEdit = new URLSearchParams(search).get("isEdit");
  const navigate = useNavigate();
  let [playerData, setPlayerData] = React.useState({});
  const [value, setValue] = React.useState(0);
  const [name, setName] = React.useState("");
  const [nickName, setNickName] = React.useState("");
  const [birthPlace, setBirthPlace] = React.useState("");
  const [currentCity, setCurrentCity] = React.useState("");
  const [DOB, setDOB] = React.useState("");
  const [age, setAge] = React.useState("");

  const [spouse, setSpouse] = React.useState("");
  const [parents, setParents] = React.useState("");

  const [email, setEmail] = React.useState("");
  const [twitter, setTwitter] = React.useState("");
  const [instagram, setInstagram] = React.useState("");

  const [school, setSchool] = React.useState("");
  const [majorSubject, setMajorSubject] = React.useState("");
  const [graduationYear, setGraduationYear] = React.useState("");
  const [golfParticipation, setGolfParticipation] = React.useState("");

  const [occupation, setOccupation] = React.useState("");
  const [company, setCompany] = React.useState("");

  const [expandedP1, setExpandedP1] = React.useState("");
  const [expandedP2, setExpandedP2] = React.useState("");
  const [expandedP3, setExpandedP3] = React.useState("");
  const [expandedP4, setExpandedP4] = React.useState("");
  const [disable, setDisable] = React.useState(true);

  const handleExpandChange = (panel) => (event, newExpanded) => {
    if (panel === "panel1") {
      setExpandedP1(newExpanded ? panel : false);
    }
    if (panel === "panel2") {
      setExpandedP2(newExpanded ? panel : false);
    }
    if (panel === "panel3") {
      setExpandedP3(newExpanded ? panel : false);
    }
    if (panel === "panel4") {
      setExpandedP4(newExpanded ? panel : false);
    }
  };

  const getProfile = async () => {
    if (profileId) {
      const response = await axios.get(
        BaseUrl + PlayerProfileEndpoint + profileId
      );
      setPlayerData(response.data);
    }

    if (isEdit === "true") {
      setDisable(false);
      handleProfileEdit();
    }
  };

  const handleChange = (event, newValue) => {
    event.preventDefault();
    setValue(newValue);
  };

  function handleSubmit(event) {
    event.preventDefault();
    console.log("Name:", name);
  }

  const handleProfileEdit = () => {
    setExpandedP1("expand");
    setExpandedP2("expand");
    setExpandedP3("expand");
    setExpandedP4("expand");
    setDisable(false);
  };

  useEffect(() => {
    getProfile();
  }, []);

  const handleRoute = (path) => navigate(path, { replace: true });

  return (
    <ThemeProvider theme={theme}>
      <DashboardLayout>
        <>
          <Box
            sx={{ m: 1, mb: 0 }}
            className="action-container margin-auto"
            style={{ border: "none" }}
          >
            <h2 style={{ marginBottom: "0", color: "#005288" }}>
              {playerData.Name}
            </h2>
          </Box>
          <div
            className="action-container margin-auto"
            style={{ borderBottom: "0.5px solid #e1e1e1", marginTop: "0" }}
          >
            <Box sx={{ width: "100%" }}>
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <CustomTabs value={value} onChange={handleChange}>
                  <Tab
                    label="Player Profile"
                    sx={{ textTransform: "none" }}
                    {...a11yProps(0)}
                  />
                  <Tab
                    label="Email History"
                    sx={{ textTransform: "none" }}
                    {...a11yProps(1)}
                  />
                  <Tab
                    label="Audit Log"
                    sx={{ textTransform: "none" }}
                    {...a11yProps(2)}
                  />
                </CustomTabs>
              </Box>
              <TabPanel value={value} index={0}>
                <Box
                  sx={{ "& > :not(style)": { m: 1 } }}
                  noValidate
                  autoComplete="off"
                >
                  <Grid
                    container
                    justifyContent="flex-end"
                    sx={{ pr: 3 }}
                    style={{ marginTop: "-10px" }}
                  >
                    {disable === true ? (
                      <Tooltip title="Edit" placement="top">
                        <IconButton
                          size="large"
                          aria-label="show more"
                          aria-haspopup="true"
                          color="inherit"
                          onClick={handleProfileEdit}
                        >
                          <EditOutlinedIcon />
                        </IconButton>
                      </Tooltip>
                    ) : (
                      <Tooltip title="View" placement="top">
                        <IconButton 
                          size="large" 
                          color="inherit"
                          onClick={() => setDisable(true)}
                        >
                          <RemoveRedEyeOutlinedIcon />
                        </IconButton>
                      </Tooltip>
                    )}
                  </Grid>
                  <form onSubmit={handleSubmit} style={{ marginTop: "0px" }}>
                    <Grid container sx={{ mt: 2 }}>
                      <Grid item xs={12} sm={6} md={4}>
                        <Grid container spacing={2}>
                          <Grid item>
                            <img src={player} alt="Player Image" height="240" />
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid container spacing={3} xs={12} sm={6} md={8}>
                        <Grid container spacing={3} sx={{ mt: 1 }}>
                          <Grid item xs={12} md={6}>
                            {disable !== true ? (
                              <FormControl fullWidth>
                                <InputLabel htmlFor="full-name">
                                  Full Name
                                </InputLabel>
                                <OutlinedInput
                                  id="full-name"
                                  value={playerData.Name}
                                  onChange={(event) => {
                                    setName(event.target.value);
                                  }}
                                  label="Full Name"
                                />
                              </FormControl>
                            ) : (
                              <>
                                <p className="custom-label">Full Name</p>
                                <div>{playerData.Name}</div>
                              </>
                            )}
                          </Grid>

                          {disable !== true ? (
                            <Grid item xs={12} md={6}>
                              <FormControl fullWidth>
                                <InputLabel htmlFor="nickName">
                                  Nickname
                                </InputLabel>
                                <OutlinedInput
                                  id="nickName"
                                  value={playerData.NickName}
                                  onChange={(event) => {
                                    setNickName(event.target.value);
                                  }}
                                  label="Nickname"
                                />
                              </FormControl>
                            </Grid>
                          ) : (
                            <>
                              {playerData?.NickName ? (
                                <Grid item xs={12} md={6}>
                                  <p className="custom-label">Nickname</p>
                                  <div>{playerData.NickName}</div>
                                </Grid>
                              ) : null}
                            </>
                          )}

                          {disable !== true ? (
                            <Grid item xs={12} md={6}>
                              <FormControl fullWidth>
                                <InputLabel htmlFor="birth-place">
                                  Place of Birth
                                </InputLabel>
                                <OutlinedInput
                                  id="birth-place"
                                  value={
                                    playerData.BirthCity +
                                    ", " +
                                    playerData.BirthState +
                                    ", " +
                                    playerData.BirthCountry
                                  }
                                  onChange={(event) => {
                                    setBirthPlace(event.target.value);
                                  }}
                                  label="Place of Birth"
                                />
                              </FormControl>
                            </Grid>
                          ) : (
                            <>
                              {playerData?.BirthCity ? (
                                <Grid item xs={12} md={6}>
                                  <p className="custom-label">
                                    {playerData.BirthCity},{" "}
                                    {playerData.BirthState},{" "}
                                    {playerData.BirthCountry}
                                  </p>
                                  <div>Totonto</div>
                                </Grid>
                              ) : null}
                            </>
                          )}
                          {disable !== true ? (
                            <Grid item xs={12} md={6}>
                              <FormControl fullWidth>
                                <InputLabel htmlFor="current-city">
                                  Current City
                                </InputLabel>
                                <OutlinedInput
                                  id="current-city"
                                  value={playerData.City}
                                  onChange={(event) => {
                                    setCurrentCity(event.target.value);
                                  }}
                                  label="Current City"
                                />
                              </FormControl>
                              </Grid>
                            ) : (
                              <>
                              {playerData?.City ? (
                                <Grid item xs={12} md={6}>
                                  <p className="custom-label"> Current City </p>
                                  <div>{playerData.City}</div>
                                </Grid>
                              ) : null }
                              </>
                            )}
                          
                          {disable !== true ? (
                            <Grid item xs={12} md={6}>
                              <FormControl fullWidth>
                                <InputLabel htmlFor="dob">Birthdate</InputLabel>
                                <OutlinedInput
                                  id="birthDate"
                                  value={playerData.BirthDate}
                                  onChange={(event) => {
                                    setDOB(event.target.value);
                                  }}
                                  label="Birthdate"
                                />
                              </FormControl>
                            </Grid>
                            ) : (
                              <>
                              {playerData?.BirthDate ? (
                                <Grid item xs={12} md={6}>
                                  <p className="custom-label">Birthdate</p>
                                  <div>{playerData.BirthDate}</div>
                                </Grid>
                              ) : null}
                              </>
                            )}

                          {disable !== true ? (
                            <Grid item xs={12} md={6}>
                              <FormControl fullWidth>
                                <InputLabel htmlFor="age">Age</InputLabel>
                                <OutlinedInput
                                  id="age"
                                  value={playerData.CalculatedAge}
                                  onChange={(event) => {
                                    setAge(event.target.value);
                                  }}
                                  label="Age"
                                />
                              </FormControl>
                              </Grid>
                            ) : (
                              <>
                                {playerData?.CalculatedAge ? (
                                  <Grid item xs={12} md={6}>
                                    <p className="custom-label">Age</p>
                                    <div>{playerData.CalculatedAge}</div>
                                  </Grid>
                                ) : null}
                              </>
                            )}
                          

                        </Grid>
                      </Grid>
                    </Grid>

                    <div>
                      <Accordion
                        expanded={
                          expandedP1 === "panel1" || expandedP1 === "expand"
                        }
                        onChange={handleExpandChange("panel1")}
                      >
                        <AccordionSummary
                          aria-controls="panel1d-content"
                          id="panel1d-header"
                        >
                          <Typography>Family</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          <Box sx={{ m: 1 }}>
                            <Grid container spacing={3}>
                              {/* <Grid item xs={12} md={6}>
                              {disable !== true ? (
                                <FormControl fullWidth>
                                  <InputLabel htmlFor="spouse">
                                    Spouse
                                  </InputLabel>
                                  <OutlinedInput
                                    id="spouse"
                                    value={spouse}
                                    onChange={(event) => {
                                      setSpouse(event.target.value);
                                    }}
                                    label="Spouse"
                                  />
                                </FormControl>
                              ) : (
                                <>
                                  <p className="custom-label">Spouse</p>
                                  <div>Preeti</div>
                                </>
                              )}
                            </Grid> */}
                              <Grid item xs={12} md={6}>
                                {disable !== true ? (
                                  <FormControl fullWidth>
                                      <TextField
                                        id="Family"
                                        label="Parents"
                                        multiline
                                        rows={5}
                                        value={playerData.FamilyMembers}
                                        onChange={(event) => {
                                          setParents(event.target.value);
                                        }}
                                      />
                                  </FormControl>
                                ) : (
                                  <>
                                    <p className="custom-label">Family</p>
                                    <div>{playerData.FamilyMembers}</div>
                                  </>
                                )}
                              </Grid>
                            </Grid>
                          </Box>
                        </AccordionDetails>
                      </Accordion>
                      <Accordion
                        expanded={
                          expandedP2 === "panel2" || expandedP2 === "expand"
                        }
                        onChange={handleExpandChange("panel2")}
                      >
                        <AccordionSummary
                          aria-controls="panel2d-content"
                          id="panel2d-header"
                        >
                          <Typography>Contact Information</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          <Grid sx={{ m: 1 }}>
                            <Grid container spacing={3}>
                              {disable !== true ? (
                                <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="email">
                                      Email
                                    </InputLabel>
                                    <OutlinedInput
                                      id="email"
                                      value={playerData.Email}
                                      onChange={(event) => {
                                        setEmail(event.target.value);
                                      }}
                                      label="Email"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                <>
                                  {playerData?.Email ? (
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">Email</p>
                                      <div>{playerData.Email}</div>
                                    </Grid>
                                  ) : null }
                                </>
                                )}

                              {disable !== true ? (
                                <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="twitter">
                                      Twitter
                                    </InputLabel>
                                    <OutlinedInput
                                      id="twitter"
                                      value={playerData.Twitter}
                                      onChange={(event) => {
                                        setTwitter(event.target.value);
                                      }}
                                      label="Twitter"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                <>
                                  {playerData?.Twitter ? (
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">Twitter</p>
                                      <div>{playerData.Twitter}</div>
                                    </Grid>
                                  ) : null }
                                </>
                              )}

                            {disable !== true ? (
                              <Grid item xs={12} md={6}>
                                <FormControl fullWidth>
                                  <InputLabel htmlFor="instagram">
                                    Instagram
                                  </InputLabel>
                                  <OutlinedInput
                                      id="instagram"
                                      value={playerData.Instagram}
                                      onChange={(event) => {
                                        setInstagram(event.target.value);
                                      }}
                                      label="Instagram"
                                    />
                                </FormControl>
                              </Grid>
                              ) : (
                                <>
                                  {playerData?.Instagram ? (
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">Instagram</p>
                                      <div>{playerData.Instagram}</div>
                                    </Grid>
                                  ) : null}
                                </>
                                )}
                            </Grid>
                          </Grid>
                        </AccordionDetails>
                      </Accordion>
                      <Accordion
                        expanded={
                          expandedP3 === "panel3" || expandedP3 === "expand"
                        }
                        onChange={handleExpandChange("panel3")}
                      >
                        <AccordionSummary
                          aria-controls="panel3d-content"
                          id="panel3d-header"
                        >
                          <Typography>Education</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          <Grid sx={{ m: 1 }}>
                            <Grid container spacing={3}>
                              
                              {disable !== true ? (
                                <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="school">
                                      Name of School Attended (or Attending)
                                    </InputLabel>
                                    <OutlinedInput
                                      id="school"
                                      value={playerData.SchoolAttending}
                                      onChange={(event) => {
                                        setSchool(event.target.value);
                                      }}
                                      label="Name of School Attended (or Attending)"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                <>
                                  {playerData?.SchoolAttending ? (
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">
                                        {" "}
                                        {playerData.SchoolAttending}
                                      </p>
                                    </Grid>
                                  ) : null}
                                </>
                              )}

                              {disable !== true ? (
                               <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="majorSubject">
                                      Major/Field of Study (List All)
                                    </InputLabel>
                                    <OutlinedInput
                                      id="majorSubject"
                                      value={playerData.SchoolMajor}
                                      onChange={(event) => {
                                        setMajorSubject(event.target.value);
                                      }}
                                      label="Major/Field of Study (List All)"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                <>
                                  {playerData?.SchoolMajor ? (
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">
                                        Major/Field of Study (List All)
                                      </p>
                                      <div>{playerData.SchoolMajor}</div>
                                    </Grid>
                                  ) : null}
                                </>
                              )}

                              {disable !== true ? (
                                <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="graduationYear">
                                      Year Graduated (or expected to graduate)
                                    </InputLabel>
                                    <OutlinedInput
                                      id="graduationYear"
                                      value={playerData?.GraduationYear}
                                      onChange={(event) => {
                                        setGraduationYear(event.target.value);
                                      }}
                                      label="Year Graduated (or expected to graduate)"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                <>
                                  {playerData?.GraduationYear ? (
                                    <Grid item xs={12} md={6}>
                                    <p className="custom-label">
                                      Year Graduated (or expected to graduate)
                                    </p>
                                    <div>{playerData?.GraduationYear}</div>
                                    </Grid>
                                  ) : null}
                                </>
                              )}

                              {disable !== true ? (
                                <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="golfParticipation">
                                      Did you participate in your schools golf
                                      team?
                                    </InputLabel>
                                    <OutlinedInput
                                      id="golfParticipation"
                                      value={golfParticipation}
                                      onChange={(event) => {
                                        setGolfParticipation(
                                          event.target.value
                                        );
                                      }}
                                      label="Did you participate in your schools golf team?"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                  <>
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">
                                        Did you participate in your schools golf
                                        team?
                                      </p>
                                      <div>Yes</div>
                                    </Grid>
                                  </>
                                )}

                            </Grid>
                          </Grid>
                        </AccordionDetails>
                      </Accordion>
                      <Accordion
                        expanded={
                          expandedP4 === "panel4" || expandedP4 === "expand"
                        }
                        onChange={handleExpandChange("panel4")}
                      >
                        <AccordionSummary
                          aria-controls="panel4d-content"
                          id="panel4d-header"
                        >
                          <Typography>Career</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          <Grid sx={{ m: 1 }}>
                            <Grid container spacing={3}>

                              {disable !== true ? (
                                <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="spouse">
                                      Occupation
                                    </InputLabel>
                                    <OutlinedInput
                                      id="occupation"
                                      value={occupation}
                                      onChange={(event) => {
                                        setOccupation(event.target.value);
                                      }}
                                      label="Occupation"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                <>
                                  {playerData?.Occupation ? (
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">Occupation</p>
                                      <div>{playerData.Occupation}</div>
                                    </Grid>
                                  ) : null}
                                </>
                              )}
                              
                              {disable !== true ? (
                                <Grid item xs={12} md={6}>
                                  <FormControl fullWidth>
                                    <InputLabel htmlFor="company">
                                      Company
                                    </InputLabel>
                                    <OutlinedInput
                                      id="company"
                                      value={company}
                                      onChange={(event) => {
                                        setCompany(event.target.value);
                                      }}
                                      label="Company"
                                    />
                                  </FormControl>
                                </Grid>
                                ) : (
                                <>
                                  {playerData?.Company ? (
                                    <Grid item xs={12} md={6}>
                                      <p className="custom-label">Company</p>
                                      <div>{playerData?.Company}</div>
                                    </Grid>
                                  ) : null}
                                  </>
                                )}

                            </Grid>
                          </Grid>
                        </AccordionDetails>
                      </Accordion>
                    </div>

                    {!disable && (
                      <Grid
                        container
                        justifyContent="end"
                        alignItems="center"
                        sx={{ mt: 2 }}
                        className="margin-auto"
                      >
                        <Stack spacing={2} direction="row">
                          <Button
                            variant="outlined"
                            onClick={() => {
                              setDisable(true);
                              handleRoute("/");
                            }}
                          >
                            Cancel
                          </Button>
                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                          >
                            Save
                          </Button>
                        </Stack>
                      </Grid>
                    )}
                  </form>
                </Box>
              </TabPanel>
              <TabPanel value={value} index={1}>
                Email History
              </TabPanel>
              <TabPanel value={value} index={2}>
                Audit Log
              </TabPanel>
            </Box>
          </div>
        </>
      </DashboardLayout>
    </ThemeProvider>
  );
};

export default PlayerProfile;
