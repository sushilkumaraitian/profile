import React from "react";
import Box from "@mui/material/Box";
import { DashboardLayout } from "../../containers/Layout/Layout";

const Merge = () => {
  return (
    <DashboardLayout>
      <Box
        sx={{ m: 1, mb: 0 }}
        className="action-container margin-auto"
        style={{ border: "none" }}
      >
        <h2 className="mb-4">Merge Page</h2>
      </Box>
    </DashboardLayout>
  );
};

export default Merge;
