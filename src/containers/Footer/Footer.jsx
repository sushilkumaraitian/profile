import React from "react";

export const Footer = () => {
  return (
    <>
      <div
        className="footer-wrapper flex flex-col flex-1"
        style={{ background: "#F7F7F9", flex: "0 0 100%", width: "100%" }}
      >
        <p className="footer-text py-2">
          2021 ©Copyright 2021 USGA. All rights reserved.
        </p>
      </div>
    </>
  );
};

export default Footer;
