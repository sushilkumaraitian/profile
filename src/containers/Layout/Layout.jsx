import React from "react";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";

import { MiniDrawer } from "../SideNav/SideNav";
import { PrimarySearchAppBar } from "../Header/Header";
import { Footer } from "../Footer/Footer";

function handleClick(event) {
  event.preventDefault();
  console.info("You clicked a breadcrumb.");
}

export const DashboardLayout = ({ children }) => {
  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      href="/"
      onClick={handleClick}
    >
      Home
    </Link>,
    <Typography key="3" color="text.primary" sx={{ fontSize: "13px" }}>
      Search
    </Typography>,
  ];

  return (
    <div className="app">
      <div className="app-header">
        <PrimarySearchAppBar />
      </div>
      <div className="app-body">
        <div className="sidebar">
          <MiniDrawer />
        </div>
        <main className="flex flex-col flex-1 overflow-hidden content main">
          <div className="content-box" style={{ padding: "5px 0px 5px 20px" }}>
            <Stack spacing={2}>
              <Breadcrumbs
                separator={<NavigateNextIcon fontSize="small" />}
                aria-label="breadcrumb"
                style={{ fontSize: "13px" }}
              >
                {breadcrumbs}
              </Breadcrumbs>
            </Stack>
          </div>
          <Divider />
          <section
            className="sm:flex-row flex flex-col flex-1"
            style={{ paddingBottom: "100px" }}
          >
            <div
              className="content-box"
              style={{ flexGrow: 2, flexBasis: "0%" }}
            >
              {children}
            </div>
          </section>
          <Footer />
        </main>
      </div>
    </div>
  );
};
