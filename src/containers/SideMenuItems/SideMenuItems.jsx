import React, { useEffect, useState } from "react";

import { styled, useTheme, ThemeProvider } from "@mui/material/styles";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import { useNavigate, useLocation } from "react-router-dom";

const CustomListItem = styled(ListItem)(({ theme }) => ({
  fontSize: "12px !important",
  "&.Mui-selected::before": {
    top: "0px",
    left: "0px",
    width: "3px",
    bottom: "0px",
    content: "''",
    display: "block",
    position: "absolute",
    borderLeft: "4px solid #00509f !important",
  },
}));

const CustomNestedListItem = styled(ListItem)(({ theme }) => ({
  fontSize: "12px !important",
  "&.Mui-selected": {
    color: "#00509f",
    backgroundColor: "transparent",
  },
}));

const useStyles = (theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    selected: true,
  },
  active: {
    color: "#DCECFB",
  },
  listItemText: {
    fontSize: "0.6em",
  },
});

const SideMenuItems = ({ closeMenu }) => {
  const theme = useTheme();
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();
  const [open, setOpen] = useState(true);
  const { pathname } = location;

  useEffect(() => {
    !closeMenu && setOpen(false);
  }, [open]);

  const handleClick = (e) => {
    e.preventDefault();
    setOpen(!open);
  };

  const handleRoute = (path) => navigate(path, { replace: true });

  return (
    <ThemeProvider theme={theme}>
      <CustomListItem
        selected={"/" === pathname}
        button
        onClick={(e) => {
          handleRoute("/");
        }}
      >
        <ListItemIcon>
          <SearchOutlinedIcon />,
        </ListItemIcon>
        <ListItemText primary="Search" />
      </CustomListItem>
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        sx={{ padding: "0px" }}
      >
        <CustomListItem
          button
          selected={"/reset" === pathname || "/merge" === pathname}
          onClick={(e) => handleClick(e)}
        >
          <ListItemIcon>
            <PersonOutlineOutlinedIcon />
          </ListItemIcon>
          <ListItemText primary="Player Profile" />
          {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
        </CustomListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <CustomNestedListItem
              button
              sx={{ pl: 4 }}
              selected={"/reset" === pathname}
              onClick={(e) => {
                handleRoute("/reset");
              }}
            >
              <ListItemIcon></ListItemIcon>
              <ListItemText
                primary="Reset Password"
                style={{ fontSize: "13px" }}
              />
            </CustomNestedListItem>
            <CustomNestedListItem
              button
              selected={"/merge" === pathname}
              sx={{ pl: 4 }}
              onClick={(e) => {
                handleRoute("/merge");
              }}
            >
              <ListItemIcon></ListItemIcon>
              <ListItemText primary="Merge" style={{ fontSize: "13px" }} />
            </CustomNestedListItem>
          </List>
        </Collapse>
      </List>
    </ThemeProvider>
  );
};

export default SideMenuItems;
