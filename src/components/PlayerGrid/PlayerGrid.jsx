import React, { useState } from 'react';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import { BaseUrl, ListingEndpoint } from '../../Constants';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

export const PlayerGrid = () => {

    const [gridApi, setGridApi] = useState(null);
    const [gridColumnApi, setGridColumnApi] = useState(null);
    let [rowData, setRowData] = useState([]);
    const pageSize = 50;
    let totalCount;
    const query = {
        "PageNumber": 1,
        "PageSize": pageSize,
        "OrderByColumn": "",
        "OrderByDir": "",
        "SearchFilters": []
    };

  
    const onGridReady = (params) => {
      setGridApi(params.api);
      setGridColumnApi(params.columnApi);
    };

    let dataSource = {
        getRows: function (params) {
            query.PageNumber = params.startRow / pageSize + 1;
            if(params.sortModel && params.sortModel.length){
                query.OrderByColumn = params.sortModel[0].colId;
                query.OrderByDir = params.sortModel[0].sort;
            }
          // Fetch our data
            const response = fetch(BaseUrl + ListingEndpoint, {
                method: 'POST',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify(query)
            }).then(response => response.json())
            .then(response => {
                totalCount = response.TotalRecords;
                const rowsThisPage = response.Data;
                const lastRow = params.endRow >= totalCount ? totalCount : -1;
                params.successCallback(rowsThisPage, lastRow);
            });
        }
      };

    // const onGridReady = (params) => {
    //     fetch(BaseUrl + ListingEndpoint, {
    //         method: 'POST',
    //         headers: { 'content-type': 'application/json' },
    //         body: JSON.stringify(query)
    //     })
    //     .then(response => response.json())
    //     .then(response => {
    //         setRowData(response.Data);
    //     });
    //   };

    const checkboxSelection = function (params) {
        return params.columnApi.getRowGroupColumns().length === 0;
    };

    const headerCheckboxSelection = function (params) {
        return params.columnApi.getRowGroupColumns().length === 0;
    };


    return (
        <div className="ag-theme-alpine" style={{ height: 'calc(100vh - 100px)', maxWidth: '100%' }}>
            <AgGridReact
                defaultColDef={{
                    flex: 1,
                    minWidth: 90,
                    resizable: true,
                    sortable: true
                }}
                components={{
                    loadingRenderer: function (params) {
                      if (params.value !== undefined) {
                        return params.value;
                      } else {
                        return '<img src="https://www.ag-grid.com/example-assets/loading.gif">';
                      }
                    },
                }}
                rowBuffer={0}
                rowSelection={'multiple'}
                rowModelType={'infinite'}
                datasource={dataSource}
                maxConcurrentDatasourceRequests={1}
                cacheBlockSize={50}
                onGridReady={onGridReady}>
                <AgGridColumn field="FirstName" cellRenderer="loadingRenderer" headerName="First Name" minWidth={170} checkboxSelection={checkboxSelection} headerCheckboxSelection={headerCheckboxSelection}></AgGridColumn>
                <AgGridColumn field="LastName" headerName="Last Name"></AgGridColumn>
                <AgGridColumn field="City" headerName="City"></AgGridColumn>
                <AgGridColumn field="State" headerName="State"></AgGridColumn>
                <AgGridColumn field="Country" headerName="Country"></AgGridColumn>
                <AgGridColumn field="Occupation" headerName="Occupation"></AgGridColumn>
                <AgGridColumn field="Company" headerName="Company"></AgGridColumn>
                <AgGridColumn field="Age" headerName="Age"></AgGridColumn>
                <AgGridColumn field="ReinstatedAmateur" headerName="Reinstated Amateur"></AgGridColumn>
            </AgGridReact>
        </div>
    );
};