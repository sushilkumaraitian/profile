export const columns = [
    {
        name: 'First Name',
        field: 'FirstName',
        sortable: true,
        selector: row => row.FirstName,
    },
    {
        name: 'Last Name',
        field: 'LastName',
        sortable: true,
        selector: row => row.LastName,
    },
    {
        name: 'City',
        field: 'City',
        sortable: true,
        selector: row => row.City,
    },
    {
        name: 'State',
        field: 'State',
        sortable: true,
        selector: row => row.State,
    },
    {
        name: 'Country',
        field: 'Country',
        sortable: true,
        selector: row => row.Country,
    },
    {
        name: 'Occupation',
        field: 'Occupation',
        sortable: true,
        selector: row => row.Occupation,
    },
    {
        name: 'Company',
        field: 'Company',
        sortable: true,
        selector: row => row.Company,
    },
    {
        name: 'Created On',
        field: 'DateAdded',
        selector: row => row.DateAddedReadable,
        sortable: true
    },
    {
        name: 'Reinstated Amateur',
        field: 'ReinstatedAmateur',
        sortable: true,
        selector: row => row.ReinstatedAmateur,
    }
];