import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { BaseUrl, ListingEndpoint } from '../../Constants';
import DataTable from 'react-data-table-component';
import CircularProgress from '@mui/material/CircularProgress';
import './PlayerTable.css';
import { useSelector, useDispatch } from 'react-redux';
import { setProfiles, selectProfiles, setSelectedChampionship, setQuery } from '../../state/profileSlice';
import { columns } from './Columns';
import { covertToReadableDate } from '../../Utils';
import { useNavigate } from "react-router-dom";

export function PlayerTable() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [selectedRows, setSelectedRows] = React.useState(false);
	const [toggledClearRows, setToggleClearRows] = React.useState(false);
	const { selectedProfiles, championships, selectedChampionship, query, profiles } = useSelector((state) => state.profile);


	const handleChange = ({ selectedRows }) => {
		setSelectedRows(selectedRows);
	};

	const covertDatesToReadableFormat = (rows) => {
		rows.forEach((row) => {
			row.DateAddedReadable = covertToReadableDate(row.DateAdded);
		});
	};

	const fetchUsers = async page => {
		setLoading(true);
		let url = BaseUrl + ListingEndpoint;
		if (query.champCode) {
			url += `?champCode=${query.champCode}`;
			if (query.champYear) {
				url += `&champYear=${query.champYear}`;
			}
		}

		const response = await axios.post(url, query);
		covertDatesToReadableFormat(response.data.Data);
		setTotalRows(response.data.TotalRecords);
		dispatch(setProfiles(response.data.Data));
		setLoading(false);
	};

	useEffect(() => {
		fetchUsers();
	}, [query]);

	if (!query.PageSize) {
		dispatch(setQuery({ ...query, PageSize: perPage }));
	}

	const handlePageChange = (page) => {
		dispatch(setQuery({ ...query, PageNumber: page }));
	};

	const handleSort = async (column, sortDirection) => {
		dispatch(setQuery({ ...query, PageNumber: 1, OrderByColumn: column.field, OrderByDir: sortDirection }));
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		dispatch(setQuery({ ...query, PageNumber: page, PageSize: newPerPage }));
	};

	const onCheckboxSelection = (selectedRows) => {
		//dispatch(selectProfiles(selectedRows));
	};

	const onSelection = (selectedRow) => {
		const updatedData = profiles.map(item => {
			if (selectedRow.Id !== item.Id) {
				return item;
			}

			return {
				...item,
				isSelected: !item.isSelected
			};
		});
		dispatch(setProfiles(updatedData));
	};

	const doubleClicked = (row) => {
		navigate("/profile/" + row.Id, { replace: true });
	};

	const conditionalRowStyles = [
		{
			when: row => row.isSelected,
			style: {
				backgroundColor: "#1976d2",
				color: "white"
			}
		}
	];

	return (
		<DataTable
			columns={columns}
			data={profiles}
			progressPending={loading}
			pagination
			paginationServer
			//selectableRows
			progressComponent={<CircularProgress />}
			paginationTotalRows={totalRows}
			//onSelectedRowsChange={onCheckboxSelection}
			selectableRowSelected={(row) => row.isSelected}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			onSort={handleSort}
			onRowDoubleClicked={doubleClicked}
			onRowClicked={onSelection}
			conditionalRowStyles={conditionalRowStyles}
			sortServer
		/>
	);
}