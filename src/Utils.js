export const covertToReadableDate = (date) => {
    return new Date(date).toDateString();
};