import './App.css';
import { NavRoutes } from './routes/NavRoutes';
import { setChampionships } from './state/profileSlice';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { BaseUrl, ChampionshipEndpoint } from './Constants';

function App() {
  const dispatch = useDispatch();
  axios.get(BaseUrl + ChampionshipEndpoint)
      .then(response => {
        dispatch(setChampionships(response.data));
      });
  return (
    <NavRoutes />
  );
}

export default App;
